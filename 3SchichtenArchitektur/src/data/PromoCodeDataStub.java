package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PromoCodeDataStub implements IPromoCodeData {

	@Override
	public boolean savePromoCode(String code) {
		
		File file = new File("Promocodes.txt");
		
		
		// creates the file
		try {
				file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// creates a FileWriter Object
		FileWriter writer;
		try {
			writer = new FileWriter(file,true);
			
			// Writes the content to the file
			writer.write(code); 
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return true;
	}

	@Override
	public boolean isPromoCode(String code) {
		return false;
	}

}
