package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;
import javax.swing.JTextArea;
import javax.swing.DropMode;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JTextArea txtarea_Promocodes;
	private int anzahl = 0;
	String code = "";

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnl_Promocodes = new JPanel();
		contentPane.add(pnl_Promocodes, BorderLayout.CENTER);
		
		txtarea_Promocodes = new JTextArea();
		txtarea_Promocodes.setEditable(false);
		txtarea_Promocodes.setColumns(60);
		txtarea_Promocodes.setTabSize(30);
		txtarea_Promocodes.setLineWrap(true);
		txtarea_Promocodes.setRows(27);
		pnl_Promocodes.add(txtarea_Promocodes);
		
		JLabel lbl_Anzahl = new JLabel("" + anzahl + "");
		lbl_Anzahl.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Anzahl.setFont(new Font("Comic Sans MS", Font.PLAIN, 11));
		pnl_Promocodes.add(lbl_Anzahl);
		
		JLabel lbl_title = new JLabel("Promotioncode Generator f\u00FCr 2-fache IT-Dollar");
		lbl_title.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 14));
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_title, BorderLayout.NORTH);
		
		JButton btn_promocodes = new JButton("Code Generieren");
		btn_promocodes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				code = new String();
				for(int i = 0; i < anzahl; i++ ) {
				 code = code + logic.getNewPromoCode() + "\n"	;
				txtarea_Promocodes.setText(code);
				
				}
			}
		});
		btn_promocodes.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 13));
		contentPane.add(btn_promocodes, BorderLayout.SOUTH);
		
		JButton btn_plus = new JButton("+");
		btn_plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				anzahl = anzahl +1;
				lbl_Anzahl.setText("" + anzahl + "");
			}
		});
		contentPane.add(btn_plus, BorderLayout.EAST);
		
		JButton btn_minus = new JButton("-");
		btn_minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(anzahl > 0)
				anzahl--;
				lbl_Anzahl.setText("" + anzahl + "");
			}
		});
		contentPane.add(btn_minus, BorderLayout.WEST);
		this.setVisible(true);
	}

}
